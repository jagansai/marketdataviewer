cmake_minimum_required(VERSION 3.10)

SET(OBJS ${CMAKE_CURRENT_SOURCE_DIR}/objs/)

include_directories(test/lib/gtest/include src/utils src/model src)
LINK_DIRECTORIES(test/lib/gtest/lib /lib/x86_64-linux-gnu/ src/model src)

add_subdirectory(src)
add_subdirectory(test)