# MarketDataViewer

Application to parse marketdata csv file and 
compute data for each ticker.   

### *Setup*   

1. Clone the repository in Linux.   
2. mkdir Release 
3. cd Release
4. export  LINK_LIB_PATH=[path to Linux libraries]
5. cmake -DCMAKE_BUILD_TYPE=Release ..
6. make
    
### *Run*   

` src/MarketDataViewer -i <path to input file> -hf Timestamp,Ticker,Bid,BidSize,Ask,AskSize,Volume`   
 ##### Output

    [A, 5.58, 0.03, 3242213583, 3872, 80.3872]   
    [AU, 0.44, 0.03, 5424205129, 3872, 22.7697]   
    [BC, 36.11, -60.97, 463120046, 3872, 78.6113]   
    [F, 0.08, 0.03, 38576098226, 3872, 24.7804]   
    [GE, 0.08, 0.03, 61598544738, 3872, 27.5405]   
    [M, 1.03, -0.08, 26758921474, 3871, 52.0559]   
    [QTM, 2.17, 0.04, 152088103, 3871, 17.078]   
    [R, 18.57, 0.04, 238389556, 3871, 93.1583]   
    [S, 0.13, 0.04, 9094917894, 3871, 21.3366]   
    [T, 0.2, 0.03, 32484957938, 3871, 48.0712]   
    [UPS, 4.84, 0.03, 2265074301, 3872, 135.923]   
### Unit Tests
1. cd test
2. ./runUnitTests   

