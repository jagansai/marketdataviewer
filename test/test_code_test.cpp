//
// Created by jagan on 18-11-2018.
//


#include <gtest/gtest.h>
#include <iostream>
#include <string>

#include <Algorithms.h>
#include <MarketData.h>
#include <ProcessMarketData.h>


auto GetMarketData() {
    std::string header = "Bid,Ask,AskSize,Timestamp,BidSize,Volume,Ticker";
    std::map<std::string, std::vector<std::shared_ptr<MarketData>>> data =
            {
                    {
                            "9101.T",
                            {
                                    std::make_shared<MarketData>(MarketData::BuildMarketData(Utils::SplitBy(header),
                                                                                             Utils::SplitBy("100,101,1000,10000,1000,14000,9101.T")))
                            }
                    },
                    {
                            "8101.T",
                            {
                                    std::make_shared<MarketData>(
                                            MarketData::BuildMarketData(Utils::SplitBy(header),
                                                                        Utils::SplitBy("100,123,1000,10003,2000,15000,    8101.T"))
                                    ),
                                    std::make_shared<MarketData>(
                                            MarketData::BuildMarketData(Utils::SplitBy(header),
                                                                        Utils::SplitBy("1000,1243,10000,10005,2000,15000, 8101.T "))
                                    ),
                                    std::make_shared<MarketData>(
                                            MarketData::BuildMarketData(Utils::SplitBy(header),
                                                                        Utils::SplitBy("1005,1243,10000,10009,2000,15000, 8101.T "))
                                    )

                            }
                    },
                    {
                            "1401.T",
                            {
                                    std::make_shared<MarketData>(
                                            MarketData::BuildMarketData(Utils::SplitBy(header),
                                                                        Utils::SplitBy("100,142,2000,10004,3500,16000,   1401.T"))
                                    )
                            }
                    }
            };
    return data;
}



TEST(TestMarketData, TEST_ORDER_OF_CSV_COLUMS) {
    MarketData m2;
   /* {
        std::string header = "Ticker,Bid,Ask,AskSize,Timestamp,BidSize,Volume";
        std::vector<std::string> data =
                {
                        "9101.T,100,101,1000,10000,1000,14000",
                        "8101.T,100,123,1000,10003,2000,15000",
                        "1401.T,100,142,2000,10004,3500,16000",
                        "8101.T,1000,1243,10000,10003,2000,15000"
                };

        m1 = MarketData::BuildMarketData(Utils::SplitBy(header), Utils::SplitBy(data[0]));
        EXPECT_EQ(m1.getAsk().Value(), 101);
    }*/
    {
        std::string header = "Bid,Ask,AskSize,Timestamp,BidSize,Volume,Ticker";
        std::vector<std::string> data =
                {
                        Utils::Strip("100,101,1000,10000,1000,14000,    9101.T"),
                        Utils::Strip("100,123,1000,10003,2000,15000,    8101.T"),
                        Utils::Strip("100,142,2000,10004,3500,16000,   1401.T"),
                        Utils::Strip("1000,1243,10000,10005,2000,15000, 8101.T ")
                };
        m2 = MarketData::BuildMarketData(Utils::SplitBy(header), Utils::SplitBy(data[0]));
        EXPECT_EQ(m2.getTicker().Value(), "9101.T");
    }
}

TEST(TEST_MaxConsecutiveTimeStampForTicker, TEST_MaxConsecutiveTimeStampForTicker_Test) {

        auto data = GetMarketData();
        auto it = data.find("8101.T");
        EXPECT_FALSE(it == data.end());
        EXPECT_EQ(ProcessMarketData::MaxTimeDiff(it->second), 4);
}

TEST(TEST_MaxAskMinusBid, TEST_MaxAskMinusBid_TEST) {
    auto data = GetMarketData();
    auto it = data.find("8101.T");
    EXPECT_FALSE(it == data.end());
    EXPECT_EQ(ProcessMarketData::MaxOfAskMinusBid(it->second), 243);
}

