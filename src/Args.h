//
// Created by jagan on 20-11-2018.
//

#ifndef PROJECT_ARGS_H
#define PROJECT_ARGS_H

#include "Commomdefs.h"

struct Args {
    bool showHelp;
    bool dryRun;
    std::string inputFile;
    std::string headerFormat;
    std::string outputFormat;

    Args() : showHelp{false}, dryRun {false} {}

    static std::string Usage() {
        std::ostringstream oss;
        oss << "MarketDataViewer -h <Help> -i <InputFile> -hf <HeaderFormat>"
            << " -of <Output Format> -dry <Dry Run>";
        return oss.str();
    }

    static Args ParseArgs(const int argc, const char* argv[]) {
        Args args;
        const std::string helpSwitch = "-h";
        const std::string inputFileSwitch = "-i";
        const std::string headerFormatSwitch = "-hf";
        const std::string outputFormatSwitch = "-of";
        const std::string dryRunSwitch = "-dry";

        for (int i = 1; i < argc;) {
            if (argv[i] == helpSwitch) {
                args.showHelp = true;
                break;
            }
            if (argv[i] == inputFileSwitch && i + 1 < argc) {
                args.inputFile = argv[i + 1];
                i += 2;
            } else if (argv[i] == headerFormatSwitch && i + 1 < argc) {
                args.headerFormat = argv[i + 1];
                i += 2;
            } else if (argv[i] == outputFormatSwitch && i + 1 < argc) {
                args.outputFormat = argv[i + 1];
                i += 2;
            } else if (argv[i] == dryRunSwitch){
                args.dryRun = true;
                ++i;
            } else {
                ++i;
            }
        }
        return args;
    }

    std::string toString() const {
        std::ostringstream oss;
        oss << "{\n\t"
            << "showHelp:" << std::boolalpha << showHelp
            << ",\n\t"
            << "dryRun:" << std::boolalpha << dryRun
            << ",\n\t"
            << "inputFile:" << inputFile
            << ",\n\t"
            << "headerFormat:" << headerFormat
            << ",\n\t"
            << "outputFormat:" << outputFormat
            << "\n}\n";
        return oss.str();
    }
};


#endif //PROJECT_ARGS_H
