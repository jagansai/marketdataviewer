//
// Created by jagan on 19-11-2018.
//

#include "ProcessMarketData.h"

auto LessThanCmp = [](MarketDataPtr const &m1, MarketDataPtr const &m2) {
    return (m1->getAsk().Value() - m1->getBid().Value()) <
           (m2->getAsk().Value() - m2->getBid().Value());
};

double ProcessMarketData::MaxOfAskMinusBid(MarketDataPtrs const &marketDataPtrs) {
    auto v = std::max_element(marketDataPtrs.begin(), marketDataPtrs.end(), LessThanCmp);
    return (*v)->getAsk().Value() - (*v)->getBid().Value();
}

double ProcessMarketData::MinOfAskMinusBid(MarketDataPtrs const &marketDataPtrs) {
    auto v = std::min_element(marketDataPtrs.begin(), marketDataPtrs.end(), LessThanCmp);
    return (*v)->getAsk().Value() - (*v)->getBid().Value();
}

unsigned long ProcessMarketData::SumOfVolume(MarketDataPtrs const &marketDataPtrs) {
    return std::accumulate(marketDataPtrs.begin(), marketDataPtrs.end(), 0ul,
                           [&](auto accumulated, const auto &m) {
                               return accumulated + m->getVolume().Value();
                           });
}

double ProcessMarketData::BidTimesAskCalc(MarketDataPtrs const &marketDataPtrs) {
    auto sum1 = 0.0;
    auto sum2 = 0.0;
    for (auto const &m : marketDataPtrs) {
        auto v1 = (m->getBid().Value() * m->getAskSize().Value()) + (m->getAsk().Value() * m->getBidSize()
                .Value());
        sum1 += v1;
        auto v2 = (m->getAskSize().Value() + m->getBidSize().Value());
        sum2 += v2;
    }
    return sum1 / sum2;
}

unsigned long ProcessMarketData::MaxTimeDiff(MarketDataPtrs const &marketDataPtrs) {
    auto first = marketDataPtrs.begin();
    auto last = marketDataPtrs.end();
    if (first == last) return 0ul;
    typedef typename std::iterator_traits<decltype(first)>::value_type value_t;

    std::vector<unsigned  long> adjacentTimeDiff(marketDataPtrs.size(), 0);

    value_t acc = (*first);
    adjacentTimeDiff[0] = acc->getTimestamp().Value();
    size_t maxTimeDiff = 0;
    size_t index = 0;
    while (++first != last) {
        value_t val = *first;
        adjacentTimeDiff[index++] = val->getTimestamp().Value() - acc->getTimestamp().Value();
        acc = std::move(val);
        maxTimeDiff = std::max(maxTimeDiff, adjacentTimeDiff[index -1]);
    }
    return maxTimeDiff;
}
