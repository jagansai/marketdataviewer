#include "Commomdefs.h"

#include "utils/TypeUtils.h"
#include "utils/Algorithms.h"

#include "model/MarketData.h"

#include "ProcessMarketData.h"
#include "Args.h"


RicMarketDataPtrsMap ReadFileHeader(const std::string &filename, const std::string &header) {
    std::ifstream inpfile{filename};
    if (!inpfile.is_open()) {
        const std::string msg = filename.empty() ? "Empty file supplied" : (filename + " not found\n");
        throw std::runtime_error(msg);
    }
    if (header.empty()) {
        throw std::runtime_error("empty header");
    }

    std::string line;
    auto headerCols = Utils::SplitBy(Utils::Strip(header, "\r"));

    std::map<std::string, std::vector<MarketDataPtr>> ricMarketDataMap;
    while (std::getline(inpfile, line)) {
        auto rowData = Utils::SplitBy(Utils::Strip(line, "\r\n"), ",", false);
        MarketDataPtr marketData = std::make_shared<MarketData>(MarketData::BuildMarketData(headerCols, rowData));
        auto it = ricMarketDataMap.find(marketData->getTicker().Value());
        if (it != ricMarketDataMap.end()) {
            it->second.push_back(marketData);
        } else {
            ricMarketDataMap.insert(std::make_pair(marketData->getTicker().Value(),
                                                   std::vector<MarketDataPtr>{marketData}));
        }
    }

    return ricMarketDataMap;
}

void ProcessData(RicMarketDataPtrsMap const &ricMarketDataPtrsMap, std::ostream &os = std::cout) {
    for (auto const &p : ricMarketDataPtrsMap) {
        auto t = std::make_tuple
                (
                        p.first,
                        ProcessMarketData::MaxOfAskMinusBid(p.second),
                        ProcessMarketData::MinOfAskMinusBid(p.second),
                        ProcessMarketData::SumOfVolume(p.second),
                        ProcessMarketData::MaxTimeDiff(p.second),
                        ProcessMarketData::BidTimesAskCalc(p.second)
                );
        using namespace Utils;
        os << t << '\n';
    }
}


int main(int argc, const char *argv[]) {
    try {
        const Args args = Args::ParseArgs(argc, argv);
        if (args.showHelp) {
            std::cout << args.Usage() << '\n';
        } else if (args.dryRun) {
            std::cout << args.toString() << '\n';
        } else {
            RicMarketDataPtrsMap ricMarketDataPtrsMap = ReadFileHeader(args.inputFile, args.headerFormat);
            ProcessData(ricMarketDataPtrsMap);
        }
    } catch (std::runtime_error &err) {
        std::cerr << err.what();
    } catch (...) {
        std::cerr << "Unknown error";
    }
}


