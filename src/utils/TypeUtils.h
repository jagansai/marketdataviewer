//
// Created by jagan on 17-11-2018.
//

#ifndef CODE_TEST_TYPEUTILS_H
#define CODE_TEST_TYPEUTILS_H

#include "../Commomdefs.h"

namespace Utils
{
    template<typename T>
    T GetType(const std::string& value, T retValue)
    {
        std::istringstream iss {value};
        iss >> retValue;
        return retValue;
    }


    inline double GetDouble(const std::string &value)
    {
        return GetType(value, 0.0);
    }

    inline int GetInt(const std::string &value)
    {
        return GetType(value, 0);
    }

    inline unsigned GetUnsigned(const std::string &value)
    {
        return GetType(value, 0u);
    }

    inline unsigned long GetUnsignedLong(const std::string &value)
    {
        return GetType(value, 0ul);
    }
}

#endif //CODE_TEST_TYPEUTILS_H
