//
// Created by jagan on 17-11-2018.
//

#ifndef CODE_TEST_NAMEDTYPES_H
#define CODE_TEST_NAMEDTYPES_H

namespace Utils
{
    template<typename T, typename Tag>
    struct NamedType
    {
    private:
        T m_value;
    public:
        NamedType() = default;
        explicit NamedType(T value)
            : m_value(value)
        {}

        const T& Value() const
        {
            return m_value;
        }

        void Value(T value)
        {
            m_value = value;
        }
    };
}

#endif //CODE_TEST_NAMEDTYPES_H
