//
// Created by jagan on 17-11-2018.
//

#ifndef CODE_TEST_ALGORITHMS_H
#define CODE_TEST_ALGORITHMS_H

#include "../Commomdefs.h"

namespace Utils {

    inline std::string Strip(std::string text, std::string const &toStrip = " ") {
        auto first = text.begin();
        auto last = text.end();
        while (true) {
            auto cur = std::find_if(first, last, [&](const char c) {
                return toStrip.find(c) != std::string::npos;
            });
            if (cur != last) {
                first = text.erase(cur);
            } else {
                break;
            }
        }
        return text;
    }

    inline std::vector<std::string> SplitBy(std::string const &line, std::string const &tokens = ",", const bool
    ignoreEmpty =
    false) {
        auto first = line.cbegin();
        auto last = line.cend();
        std::vector<std::string> result;

        while (true) {
            auto cur = std::find_if(first, last, [&](const char c) {
                return tokens.find_first_of(c) != std::string::npos;
            });
            if (!ignoreEmpty || std::distance(first, cur) != 0)
                result.emplace_back(std::string{first, cur});
            if (cur == last)
                break;
            first = std::next(cur);
        }
        return result;
    }

    // Code to pretty print tuple.

    template<typename Tuple, size_t N>
    struct TuplePrinter {

        static void Print(Tuple const& t, std::ostream& os, size_t numOfTuples) {
            TuplePrinter<Tuple, N-1>::Print(t, os, numOfTuples);
            os << ", " << std::get<N-1>(t);
        }
    };

    template<typename Tuple>
    struct TuplePrinter<Tuple, 1> {
        static void Print(Tuple const& t, std::ostream &os, size_t) {
            os << std::get<0>(t);
        }
    };


// pretty-print a tuple
    template<class Ch, class Tr, class... Args>
    auto& operator<<(std::basic_ostream<Ch, Tr>& os,
                     const std::tuple<Args...>& t)
    {
        os << "[";
        TuplePrinter<decltype(t), sizeof...(Args)>::Print(t, os, sizeof...(Args));
        return os << "]";
    }
} // end of namespace.



#endif //CODE_TEST_ALGORITHMS_H
