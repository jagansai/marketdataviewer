//
// Created by jagan on 17-11-2018.
//

#ifndef CODE_TEST_MARKETDATA_H
#define CODE_TEST_MARKETDATA_H

#include <ostream>
#include "../Commomdefs.h"
#include "../utils/NamedTypes.h"

using Timestamp = Utils::NamedType<unsigned long, struct TimestampTag>;
using Ticker = Utils::NamedType<std::string, struct TickerTag>;
using Bid = Utils::NamedType<double, struct BidTag>;
using BidSize = Utils::NamedType<unsigned, struct BidSizeTag>;
using Ask = Utils::NamedType<double, struct AskTag>;
using AskSize = Utils::NamedType<unsigned, struct AskSizeTag>;
using Volume = Utils::NamedType<unsigned long, struct VolumeTag>;

class MarketData {

public:
    MarketData(const Timestamp &timestamp, const Ticker &ticker, const Bid &bid, BidSize m_bidSize,
               const Ask &ask, const AskSize &askSize, const Volume &volume);

    MarketData() = default;

    static MarketData BuildMarketData(std::vector<std::string> const& headerCols, std::vector<std::string> const&
            rowData);


    const Timestamp &getTimestamp() const {
        return m_timestamp;
    }

    const Ticker &getTicker() const {
        return m_ticker;
    }

    const Bid &getBid() const {
        return m_bid;
    }

    MarketData &setBidSize(const BidSize &bidSize);

    const BidSize &getBidSize() const {
        return m_bidSize;
    }

    const Ask &getAsk() const {
        return m_ask;
    }

    const AskSize &getAskSize() const {
        return m_askSize;
    }

    const Volume &getVolume() const {
        return m_volume;
    }

    MarketData &setTimestamp(const Timestamp &timestamp);

    MarketData &setTicker(const Ticker &ticker);

    MarketData &setBid(const Bid &bid);

    MarketData &setAsk(const Ask &ask);

    MarketData &setAskSize(const AskSize &askSize);

    MarketData &setVolume(const Volume &volume);

    bool operator==(const MarketData &rhs) const;

    bool operator!=(const MarketData &rhs) const;

    friend std::ostream &operator<<(std::ostream &os, const MarketData &data);

private:
    Timestamp m_timestamp;
    Ticker m_ticker;
    Bid m_bid;
    BidSize m_bidSize;
    Ask m_ask;
    AskSize m_askSize;
    Volume m_volume;


};


#endif //CODE_TEST_MARKETDATA_H
