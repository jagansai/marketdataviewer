//
// Created by jagan on 17-11-2018.
//

#include <tuple>
#include "MarketData.h"
#include "../utils/TypeUtils.h"

MarketData::MarketData(const Timestamp &timestamp, const Ticker &ticker, const Bid &bid, BidSize m_bidSize,
                       const Ask &ask, const AskSize &askSize, const Volume &volume)
        : m_timestamp(timestamp), m_ticker(ticker),
          m_bid(bid), m_bidSize(m_bidSize), m_ask(ask), m_askSize(askSize),
          m_volume(volume) {}

MarketData &MarketData::setTimestamp(const Timestamp &timestamp) {
    m_timestamp = timestamp;
    return *this;
}

MarketData &MarketData::setTicker(const Ticker &ticker) {
    m_ticker = ticker;
    return *this;
}

MarketData &MarketData::setBid(const Bid &bid) {
    m_bid = bid;
    return *this;
}

MarketData &MarketData::setBidSize(const BidSize &bidSize) {
    m_bidSize = bidSize;
    return *this;
}

MarketData &MarketData::setAsk(const Ask &ask) {
    m_ask = ask;
    return *this;
}

MarketData &MarketData::setAskSize(const AskSize &askSize) {
    m_askSize = askSize;
    return *this;
}

MarketData &MarketData::setVolume(const Volume &volume) {
    m_volume = volume;
    return *this;
}

bool MarketData::operator==(const MarketData &rhs) const {
    return std::tie(m_timestamp.Value(), m_ticker.Value(), m_bid.Value(), m_bidSize.Value(), m_ask.Value(),
                    m_askSize.Value(), m_volume.Value()) ==
           std::tie(rhs.m_timestamp.Value(), rhs.m_ticker.Value(), rhs.m_bid.Value(), rhs.m_bidSize.Value(),
                    rhs.m_ask.Value(), rhs.m_askSize.Value(), rhs.m_volume.Value());
}

bool MarketData::operator!=(const MarketData &rhs) const {
    return !(rhs == *this);
}

// build marketdata.

MarketData MarketData::BuildMarketData(std::vector<std::string> const &headerCols,
                                       std::vector<std::string> const &rowData) {
    enum Cols : int {
        TimestampEnum,
        TickerEnum,
        BidEnum,
        BidSizeEnum,
        AskEnum,
        AskSizeEnum,
        VolumeEnum
    };
    static auto headerColsMap = [&]() {
        const std::unordered_map<std::string, Cols> m =
                {
                        {"Timestamp", Cols::TimestampEnum},
                        {"Ticker",    Cols::TickerEnum},
                        {"Bid",       Cols::BidEnum},
                        {"BidSize",   Cols::BidSizeEnum},
                        {"Ask",       Cols::AskEnum},
                        {"AskSize",   Cols::AskSizeEnum},
                        {"Volume",    Cols::VolumeEnum}
                };


        std::vector<Cols> headerColsPair;
        for (auto const &col : headerCols) {
            auto it = m.find(col);
            if (it == m.end()) {
                throw std::runtime_error("invalid col:" + col);
            }
            headerColsPair.emplace_back(it->second);
        }
        return headerColsPair;
    }();

    MarketData marketData;
    for (size_t index = 0; index < rowData.size(); ++index) {
        switch (headerColsMap[index]) {
            case Cols::TimestampEnum:
                marketData.setTimestamp(Timestamp{Utils::GetUnsignedLong(rowData[index])});
                break;
            case Cols::AskEnum:
                marketData.setAsk(Ask{Utils::GetDouble(rowData[index])});
                break;
            case Cols::AskSizeEnum:
                marketData.setAskSize(AskSize{Utils::GetUnsigned(rowData[index])});
                break;
            case Cols::BidEnum:
                marketData.setBid(Bid{Utils::GetDouble(rowData[index])});
                break;
            case Cols::BidSizeEnum:
                marketData.setBidSize(BidSize{Utils::GetUnsigned(rowData[index])});
                break;
            case Cols::TickerEnum:
                marketData.setTicker(Ticker{rowData[index]});
                break;
            case Cols::VolumeEnum:
                marketData.setVolume(Volume{Utils::GetUnsignedLong(rowData[index])});
                break;
        }
    }
    return marketData;
}

std::ostream &operator<<(std::ostream &os, const MarketData &data) {
    os << "m_timestamp: " << data.m_timestamp.Value() << " m_ticker: " << data.m_ticker.Value() << " m_bid: "
       << data.m_bid.Value()
       << " m_bidSize: " << data.m_bidSize.Value() << " m_ask: " << data.m_ask.Value() << " m_askSize: "
       << data.m_askSize.Value()
       << " m_volume: " << data.m_volume.Value();
    return os;
}
