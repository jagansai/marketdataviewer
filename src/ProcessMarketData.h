//
// Created by jagan on 19-11-2018.
//

#ifndef CODE_TEST_PROCESSMARKETDATA_H
#define CODE_TEST_PROCESSMARKETDATA_H


#include "model/MarketData.h"

using MarketDataPtr = std::shared_ptr<MarketData>;
using MarketDataPtrs = std::vector<MarketDataPtr>;
using RicMarketDataPtrsMap = std::map<std::string, MarketDataPtrs>;

struct ProcessMarketData {
    static double MaxOfAskMinusBid(MarketDataPtrs const &marketDataPtrs);

    static double MinOfAskMinusBid(MarketDataPtrs const &marketDataPtrs);

    static unsigned long SumOfVolume(MarketDataPtrs const &marketDataPtrs);

    static double BidTimesAskCalc(MarketDataPtrs const &marketDataPtrs);

    static unsigned long MaxTimeDiff(MarketDataPtrs const &marketDataPtrs);
};


#endif //CODE_TEST_PROCESSMARKETDATA_H
